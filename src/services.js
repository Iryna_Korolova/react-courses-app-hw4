import axios from 'axios';

const api = axios.create({
	baseURL: 'http://localhost:3000/',
});

api.interceptors.request.use((config) => {
	config.headers.Authorization = localStorage.getItem('token');
	return config;
});

api.interceptors.response.use(
	(response) => Promise.resolve(response.data),
	(error) => Promise.reject(error)
);

export function getCourses() {
	return api.get(`/courses/all`);
}

export function removeCourse(courseId) {
	return api.delete(`/courses/${courseId}`);
}

export function updateCourse(courseId, courseUpdate) {
	return api.put(`/courses/${courseId}`, courseUpdate);
}

export function newCourse(course) {
	return api.post(`/courses/add`, course);
}

export function getAllAuthors() {
	return api.get(`/authors/all`);
}

export function addAuthor(author) {
	return api.post(`/authors/add`, author);
}

export function logInUser(loginData) {
	return api.post(`/login`, loginData);
}

export function logOutUser() {
	return api.delete(`/logout`);
}

export function getCurrentUser() {
	return api.get(`/users/me`);
}
