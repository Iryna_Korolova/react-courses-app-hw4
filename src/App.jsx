import './App.css';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';

function App() {
	return (
		<Provider store={store}>
			<Router>
				<Header />
				<Routes>
					<Route path='/courses/*' element={<Courses />}></Route>
					<Route path='/login' element={<Login />}></Route>
					<Route path='/registration' element={<Registration />}></Route>
				</Routes>
			</Router>
		</Provider>
	);
}

export default App;
