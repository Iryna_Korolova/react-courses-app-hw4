import { addAuthor, getAllAuthors } from '../../services';
import { addNewAuthor, setAllAuthors } from './actionCreators';

export function fetchAllAuthors() {
	return (dispatch) => {
		getAllAuthors()
			.then((authors) => dispatch(setAllAuthors(authors.result)))
			.catch((err) => {
				console.log(err);
			});
	};
}

export function fetchAuthor(author) {
	return (dispatch) => {
		addAuthor(author)
			.then((newAuthor) => dispatch(addNewAuthor(newAuthor.result)))
			.catch((err) => {
				console.log(err);
			});
	};
}
