import { LOGIN, LOGOUT, SET_USER_DATA } from './actionTypes';

const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export function userReducer(state = initialState, action) {
	switch (action.type) {
		case LOGIN:
			return { isAuth: true, token: action.payload };
		case LOGOUT:
			return { isAuth: false, name: '', email: '', token: '', role: '' };
		case SET_USER_DATA:
			return { ...state, ...action.payload };
		default:
			return state;
	}
}
