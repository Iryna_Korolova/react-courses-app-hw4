import { LOGIN, LOGOUT, SET_USER_DATA } from './actionTypes';

export function logIn(token) {
	return {
		type: LOGIN,
		payload: token,
	};
}
export function logOut() {
	return {
		type: LOGOUT,
	};
}
export function setUser(user) {
	console.log(user);
	return {
		type: SET_USER_DATA,
		payload: user,
	};
}
