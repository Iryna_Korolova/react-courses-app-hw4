import './CourseCard.css';

import Button from '../../../common/Button/Button';

import { pipeDuration } from '../../../helpers/pipeDuration';
import { dateGenerator } from '../../../helpers/dateGenerator';

import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { authorsSelector } from '../../../store/authors/selectors';
import { fetchRemoveCourse } from '../../../store/courses/thunk';
import { userSelector } from '../../../store/user/selectors';

export default function CourseCard({ course }) {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const authors = useSelector(authorsSelector);
	const user = useSelector(userSelector);

	const authorNames = course.authors
		.map((authorId) => authors.find((author) => author.id === authorId)?.name)
		.join(', ');
	return (
		<div className='card'>
			<div className='card-inner-description'>
				<h2 className='card-title'>{course.title}</h2>
				<p className='card-text'>{course.description}</p>
			</div>
			<div className='card-inner-info'>
				<h3 className='card-info card-authors'>Authors: {authorNames}</h3>
				<h3 className='card-info'>Duration: {pipeDuration(course.duration)}</h3>
				<h3 className='card-info'>
					Created: {dateGenerator(course.creationDate)}
				</h3>
				<div className='card-btn-wrap'>
					<Button
						buttonText='Show course'
						onClick={() => navigate(`/courses/${course.id}`)}
					/>

					{user.role === 'admin' && (
						<>
							<Button
								buttonText='&#9998;'
								onClick={() => navigate(`/courses/update/${course.id}`)}
							></Button>
							<Button
								buttonText='&#128465;'
								onClick={() => dispatch(fetchRemoveCourse(course.id))}
							></Button>
						</>
					)}
				</div>
			</div>
		</div>
	);
}
